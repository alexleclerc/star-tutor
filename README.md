# Star Tutor
This project is Alex LeClerc's individual portion of the senior project capstone at Western Oregon University. It contains the source code and documentation of Star Tutor, an android application that teaches amateur astronomers how to identify constellations and celestial objects.  
It is written in Kotlin and is developed using Agile development practices.

## Vision Statement
For amateur astronomers who want to practice their constellation identification, the Star Tutor app is an Android App written in Kotlin that will train users to familiarize and identify constellations, celestial bodies, and other astronomical items. The app present users with a curriculum containing short, discrete sections that will build the users knowledge over time, in a similar fashion to Duolingo or SoloLearn. The individual lessons will contain reading, examples, and problem solving sections to encourage active learning.  The format will allow users to build their knowledge in convenient bites. Unlike current astronomy apps, this application will promote the users expanding their own knowledge of astronomy rather than relying on the app to do the indentification. 

## Documents
* [Needs and Features](documentation/NeedsFeatures.md)
* [Requirements](documentation/Requirements.md)