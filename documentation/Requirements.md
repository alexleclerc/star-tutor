# Star Tutor
## Requirements
* Data analysis on user progress/activity
* Interface that includes an overview of user progress and available lessons
* A database that stores information from a Star Catalog
* A procedure to parse the star catalog into the database 
* Use of a star catalog, matching the X, Y, and Z coordinates and the magnitude of each star, and assembling the stars that make up a constellation by their unique identifier. 
* Constellation rendering with data from Star Catalog to dynamically draw constellations only when they need to be displayed
* Different constellation drawing mode renderers for easy, medium, and hard difficulty
* Track user progress through curriculum 
* Organization of lessons by level, and questions they include
