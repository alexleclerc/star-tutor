# Star Tutor
## Needs / Features
* Track users progress through curriculum.  
* Splash screen that contains lessons and information on progress.  
    * Suggestions on subjects to revisit  
* Quiz at the end of each lesson section.  
* Lessons graded by review quiz at the end  
* Suggestion of moving on / repeating depending on score  
* A 'flash card' section for constellations, with different levels of difficulty (easy being constellations with images drawn over, medium being constellations connect by lines, hard being just the arrangement of stars)
